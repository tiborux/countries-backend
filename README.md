## How to install

1. First clone this repo in your computer: git clone https://tiborux@bitbucket.org/tiborux/countries-backend.git

2. Open the proyect with your ide (intellij, eclipse) and run the proyect.

3. You can access to the api entering in: http://localhost:8080/swagger-ui.html or change the port to your custom port.

You can create a jar and execute the jar:

1. With the proyect downloaded, install maven.
2. Go to the root folder proyect and use: mvn package
3. You will see something like this: [INFO] Building jar: /Users/Beltran/IdeaProjects/Country/target/countries-rest-1.0.jar
4. Now open and terminal and execute: java -jar /Users/Beltran/IdeaProjects/Country/target/countries-rest-1.0.jar (change to your path)


