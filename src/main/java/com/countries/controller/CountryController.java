package com.countries.controller;

import com.countries.model.Country;
import com.countries.service.CountryRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@CrossOrigin()
@RestController
@RequestMapping(value = "/countries")
@Api(value="Country", description="Operations in country list")
public class CountryController {
    @Autowired // This means to get the bean called userRepository
    // Which is auto-generated by Spring, we will use it to handle the data
    private CountryRepository countryRepository;

    @ApiOperation(value = "View a list of countries",response = Iterable.class)
    @GetMapping
    public Iterable<Country> getCountries() {
        // This returns a JSON or XML with the users
        return countryRepository.findAll();
    }

    @ApiOperation(value = "Add a country",response = Country.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Added country"),
            @ApiResponse(code = 500, message = "Internal server error") })
    @PostMapping
    public Country addCountry(@Valid @RequestBody Country country) {
        return countryRepository.save(country);
    }


    @ApiOperation(value = "Update a country", response=Country.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Updated country"),
            @ApiResponse(code = 404, message = "Country with given id does not exist"),
            @ApiResponse(code = 500, message = "Internal server error") })
    @PutMapping("/{id}")
    public ResponseEntity<Country> updateCountry(@PathVariable(value = "id") Long countryId,
                                                 @Valid @RequestBody Country countryDetails) {
        Country country = countryRepository.findOne(countryId);
        if(country == null) {
            return ResponseEntity.notFound().build();
        }
        country.setCountryName(countryDetails.getCountryName());
        country.setPopulation(countryDetails.getPopulation());


        Country updatedCountry = countryRepository.save(country);
        return ResponseEntity.ok(updatedCountry);
    }

    @ApiOperation(value = "Delete a country")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Deleted country"),
            @ApiResponse(code = 404, message = "Country with given id does not exist"),
            @ApiResponse(code = 500, message = "Internal server error") })
    @DeleteMapping("/{id}")
    public ResponseEntity<Country> deleteCountry(@PathVariable(value = "id") Long countryId) {
        Country country = countryRepository.findOne(countryId);
        if(country == null) {
            return ResponseEntity.notFound().build();
        }
        countryRepository.delete(country);
        return ResponseEntity.ok().build();
    }


}
