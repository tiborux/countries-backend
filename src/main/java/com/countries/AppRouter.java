package com.countries;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class AppRouter {

    @RequestMapping(value = "/", method = RequestMethod.GET)
    String home() {
        return "/countries";
    }

}